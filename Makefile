all: test

test: board.c main.c mozaic.h
	gcc -g board.c main.c -o test -lm -lepoxy -lglfw

clean:
	rm -rf *.o test
