#include "mozaic.h"

float *vertices;
static int vbuff_idx = 0;

typedef struct {
  double x;
  double y;
} Node;

#define DISTANCE(a, b) sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2))

double find_angle(double c1, double c2, double f) {
  double num = pow(c1, 2) + pow(c2, 2) - pow(f, 2);
  double denom = 2 * c1 * c2;
  return acos(num / denom);
}

Node generate_new_node(Node *nodes, int *idx_ret) {
  // Node nodes[3] = {*a, *b, *c};
  int idx = rand() % 3;

  Node sel, n1, n2;
  sel = nodes[idx % 3];
  n1 = nodes[(idx + 1) % 3];
  n2 = nodes[(idx + 2) % 3];

  double n1_len, n2_len, sel_len;
  n2_len = DISTANCE(sel, n1);
  n1_len = DISTANCE(sel, n2);
  sel_len = DISTANCE(n1, n2);

  double theta = find_angle(n1_len, n2_len, sel_len);
  theta /= 2;

  // calculate the equation of the far side
  double m_sel, m_n2, m_new, b_sel, b_n2, b_new;
  m_sel = (n1.y - n2.y) / (n1.x - n2.x);
  b_sel = n1.y - (m_sel * n1.x);

  if (n1.x == n2.x) {
    m_sel = 100000000.0;
    b_sel = n1.x;
  }

  // calculate the equation of the near side
  m_n2 = (n1.y - sel.y) / (n1.x - sel.x);
  b_n2 = n1.y - (m_n2 * n1.x);

  if (n1.x == sel.x) {
    m_n2 = 100000000.0;
    b_n2 = n1.x;
  }

  // extrapolate the new equation
  m_new = (tan(theta) + m_n2) / (1 - (m_n2 * tan(theta)));
  b_new = sel.y - (m_new * sel.x);

  Node new;
  new.x = (b_new - b_sel) / (m_sel - m_new);
  new.y = m_sel *new.x + b_sel;

  *idx_ret = idx;
  return new;
}

void create_triangles(Node a, Node b, Node c, int level) {
  Node nodes[3] = {a, b, c};
  int idx;
  Node ret = generate_new_node(nodes, &idx);

  if (level == DEPTH) {
    vertices[vbuff_idx++] = a.x;
    vertices[vbuff_idx++] = a.y;
    vertices[vbuff_idx++] = 0.0f;
    vertices[vbuff_idx++] = b.x;
    vertices[vbuff_idx++] = b.y;
    vertices[vbuff_idx++] = 0.0f;
    vertices[vbuff_idx++] = c.x;
    vertices[vbuff_idx++] = c.y;
    vertices[vbuff_idx++] = 0.0f;
  } else {
    create_triangles(nodes[idx % 3], nodes[(idx + 1) % 3], ret, level + 1);
    create_triangles(nodes[(idx + 2) % 3], nodes[(idx) % 3], ret, level + 1);
  }
}

void generate_vertices(void) {
  srand(time(NULL));

  int size = (int)pow(2, DEPTH) * 9;
  printf("%d\n", size);
  vertices = (float *)malloc(sizeof(float) * size);
  printf("Yo\n");
  Node a, b, c;
  a.x = 50.0;
  a.y = 100.0;
  b.x = 0.0;
  b.y = 0.0;
  c.x = 100.0;
  c.y = 0.0;

  create_triangles(a, b, c, 0);
  printf("%f %f %f\n", vertices[0], vertices[1], vertices[2]);
  printf("Ay\n");
}
